# Instrucciones lógica de resolución
Debido a que la descripción de una serie de sucesión Fibonacci es la suma de los dos número anteriores empezando la serie con 0 y 1 en esas respectivas posiciones.
Podemos resolver este problema realizando una itereación con un ciclo for en el que el contador inicialize en 2 ya que la posición 0 y 1 estan ocupadas por el inicio de la serie e iterarlo hasta que el valor del contador sea igual al indice que deseamos conocer, almacenando en cada interación el valor del indice actual del fibonnaci y el valor de un indice anterior para que de esta manera podamos realizar la siguiente secuencia de fibonacci.

## Instrucciones
- Requieres tener instalado Gradle 8.3 o superior

Ejecutamos el proyecto con ./gradlew bootRun

Una vez iniciado el proyecto, estara corriendo en el puerto 8080, podemos probar el servicio con el siguiente request.

GET - localhost:8080/v1/fibonacci?indice=6 -------> Retorna 8