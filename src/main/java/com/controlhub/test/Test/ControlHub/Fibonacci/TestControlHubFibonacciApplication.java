package com.controlhub.test.Test.ControlHub.Fibonacci;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestControlHubFibonacciApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestControlHubFibonacciApplication.class, args);
	}

}
