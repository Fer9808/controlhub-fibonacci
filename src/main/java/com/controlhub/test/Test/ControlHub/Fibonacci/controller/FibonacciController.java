package com.controlhub.test.Test.ControlHub.Fibonacci.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.controlhub.test.Test.ControlHub.Fibonacci.service.FibonacciService;


@RestController
@RequestMapping(value = "v1")
public class FibonacciController {
	private static final Logger LOG = LoggerFactory.getLogger(FibonacciController.class);
	
	private static final String RESPONSE = "El valor del indice es: ";
	
	@Autowired
	FibonacciService fibonacciService;
	
	@GetMapping("fibonacci")
	public ResponseEntity<String> getValueIndex(
			@RequestParam(name = "indice") int n
			) {
		LOG.info("FibonacciController - getValueIndex :: Initialize");
		long responseValue = fibonacciService.getValueIndex(n);
		LOG.info("FibonacciController - getValueIndex :: Finish");
		return new ResponseEntity<>(RESPONSE + responseValue, HttpStatus.OK);
	}
}
