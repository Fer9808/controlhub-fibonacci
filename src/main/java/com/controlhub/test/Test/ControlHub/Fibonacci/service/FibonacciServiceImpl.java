package com.controlhub.test.Test.ControlHub.Fibonacci.service;

import org.springframework.stereotype.Service;

@Service
public class FibonacciServiceImpl implements FibonacciService {
	@Override
	public long getValueIndex(int index) {
		return calcularFibonacci(index);
	}

	public static long calcularFibonacci(int indice) {
		if (indice <= 1) {
            return indice;
        }

        long actual = 1;
        long previo = 1;

        for (int contador = 2; contador < indice; contador++) {
            long temporal = actual;
            actual += previo;
            previo = temporal;
        }
        return actual;
    }
}
